/* 

OPERATORS
	-Assignment (=)
	-Arithmetic (+ - / *)
	-Compound Assignment 
	-Comparison
	-Logical

1. Assignment Operator (=)
	-use to assign a value to a variable

	example:

	let a=14;
	console.log(a);

	a=7;
	console.log(a);

	let b=a
	console.log(a, b);

2. Arithmetic Operator (+ - / * %)

	Example:
	console.log(20+10);
	console.log(20-10);
	console.log(20*10);
	console.log(100/3);
	console.log(100%3); - MODULO (REMAINDER)

	INCREMENT (++) & DECREMENT (--)

	let c=30;
	// prefix
	console.log(++c); 	//31
	console.log(c);		//31
	console.log(--c);	//30
	console.log(c);		//30
	// suffix
	console.log(c++);	//30
	console.log(c);		//31
	console.log(c--)	//31
	console.log(c);		//30


	Mini Activity

	console.log(5+5);

	let x=8;
	y=8;
	console.log(x+y);

	let a=10;
	let b=5;
	let c=2;
	console.log((a-b)*c);

3. Compound Assignment
	-perform arithmetic operation
	-assigning back the value to the variable

	Example:
	
	Addidtion Ass Oper (+=)
	let a=15;
	console.log(a+=3);	//18
	console.log(a+=3);	//21
	console.log(a+=3);	//24

	// a=a+3
	// a=15+3

	// a=a+3
	// a=18+3
	// a=24

	console.log(a+3);	//27
	console.log(a+3);	//27
	console.log(a+3);	//27

	Subtraction Ass Oper (-=)
	let a=15;
	console.log(a+=3);	//18
	console.log(a+=3);	//21
	console.log(a+=3);	//24

	console.log(a-=2);	//22
	console.log(a-=2);	//20
	console.log(a-=5);	//15
	console.log(a);

	Multi and Division
	let a=15;
	console.log(a*=2);	//30
	console.log(a*=3);	//90
	console.log(a*=4);	//360
	console.log(a);

	console.log(a/=10);	//36
	console.log(a/=4);	//9
	console.log(a/=3);	//3
	console.log(a);
	
	Modular
	let f=200;
	console.log(f%=11); //2
	console.log(f%=1);	//0	

4. Comparison Operators
	-compare two operands and returns a logical value (boolean value)
	-Equality (==)
	-Inequality (!=)
	-Strict equality (===)
	-Strict Inequality (!==)

-Euality Operator (==)
		-sometimes loose equality operator
		-evalutaes values
		-check the data types of operands
		-convert/type coercion if values have different data types


	TYPE COERCION applies to equal and inequality only.

		parseInt()
			-use to convert a data into an integer/number

		parseFloat()
			-use to convert a data into a number while keeping its decimal places

		toFixed()
			-sets the no of decimal places
			-returns a string

		console.log(typeof("Jan" == "Jan"));
		console.log("Jan" == "Jan");
		console.log(true == true);
		console.log("yes" == "Yes");
		console.log(true == false);
		console.log(null == undefined);
		console.log(true == "2");
		console.log("100");
		console.log( parseInt("100"));
		console.log(100)
		console.log("94.63")
		console.log(94.63)
		console.log( parseInt("94.63"));
		console.log( parseFloat("94.63"));
		console.log(3.14159265);
		console.log( 3.14159265.toFixed(3));
		console.log( parseFloat(3.14159265.toFixed(3)));

-Inequlity Operator ( != )
		-not equal operands
		
		console.log("James" != "Daniel");
		console.log(3.00 !== 3 );
		console.log(3.01 !== 3 );
		console.log(false != true);
		console.log(true != "true");
		console.log("true" * 1);
		console.log(null != undefined)
		console.log(false != 1);

		console.log(parseInt(true));
		console.log(parseInt("true"));

-STrict Equality (===)
	-compares the sameness of values & data type
	-type coercion doesnt apply

		console.log(1 === 1);	//true
		console.log(2 === "2");	//false
		console.log(true === 1);	//false
		console.log(null === undefined); //false

-Strict InEquality (!==)
	-compares the sameness of values & data type not equal
	-type coercion doesnt apply

		console.log(4 !== 4);
		console.log("4" !== "four");
		console.log(undefined !== null);


-Relational Operators ( < > >= <=)
		-greater than
		-less than
		-greater than or equal to
		-less than or eqyal to

		console.log(45>50);
		console.log(50>49);
		console.log(5.00 >= 5);
		console.log(70 >= 71)
		console.log(45<50);
		console.log(50<49);	
		console.log(5.00 <= 5);
		console.log(70 <= 71)


5. Logical Operators
		-evaluate operands
		-returns boolean value

-AND operator ( && )
		-checks whether all operands are true value

		console.log(true && true);
		console.log(true && false);
		console.log(false && true);
		console.log(false && false);

		let g=true;
		h=false;
		i=12;

		console.log(i<2);
		console.log(g==h);
		console.log(g && h);

-OR operator (||)
		-checks either one of the operands is in true value

		console.log(true || true);
		console.log(true || false);
		console.log(false || true);
		console.log(false || false);

		let j=3;
			k=4

		console.log(j>7 || 8<k) //false

-NOT Operator (!)
		-negates a value

		console.log(!true);
		console.log(!false);

		let isStudent = true;
		isInstructor = "1";

		let isAnswer = !isStudent == isInstructor;
		console.log(isAnswer);

*/

/* SELECTION CONTROL STRUCTURE
		-if else statement
		-switch case

1. IF ELSE
	
		syntax:
		if(condition){
			//statement
		}

		let age=20;

		if(age>18){
			console.log(`You are allowed to enter`);
		}else{
			console.log(`You are not allowed to enter`);
		}

	Mini Activity:
		
	Sol#1
		let h=100;

		if(h>=150){
			alert(`Above 150cm`);
		}else{
			alert(`Below 150cm`);
		}

	Sol#2
		function checkHeight(height){


			if(height>=150){
				alert(`Above 150cm`);
			}else{
				alert(`Below 150cm`);
			}
		}

		checkHeight(150)
		
	SOl#3
		function checkHeight(height){


			if(height>=150){
				alert(`Above 150cm`);
			}else{
				alert(`Below 150cm`);
			}
		}

		let result=checkHeight(100)

	Sol#4
		function checkHeight(height){


			if(height>=150){
				alert(`Above 150cm`);
			}else{
				alert(`Below 150cm`);
			}
		}

		let myHeight = prompt('Enter your height')

		checkHeight(myHeight)
		
	Sol#5
		function findName(name){


			if(name=="Romeo"){
				console.log("Juliet");
			}else{
				console.log("Hamlet");
			}

		}

		findName("Romeo");


2. IF...ELSE IF...ELSE STATEMENT

	Example:
		let price=1000;

		if(price<=20){
			console.log(`Affordable`);
		}else if(price<=100){
			console.log(`Pricey but still affordable`);
		}else if(price<=200){
			console.log(`Not affordable at the moment`);
		}else{
			console.log(`Not affordable at all`)
		}


	Example:
		let price=250;

		if(price<=20){
			console.log(`Affordable`);
		} 

		if(price > 20 && price <=100){
			console.log(`Pricey but still affordable`);
		}

		if(price>100 && price <=200){
			console.log(`Not affordable at the moment`);
		}

		if(price > 200){
			console.log(`Not affordable at all`);
		}

	Determin the typhoon intensity with the following data:

	1. Windspeed of 30, display not a typhoon
	2. Windspeed of 61, display a tropical depression is detected
	3 Windspeed of 62-88, display a tropical storm is detected 
	4. Windspeed 89-117, display a severe tropical storm is detected
	5. beyond any windspeed, display cannot identify storm detected

	Sol#1	
	let windSpeed=118;

	if(windSpeed <= 30){
		console.log(`Not a typhoon`);
	}else if(windSpeed <= 61){
		console.log(`Tropical Depresson is detected`);
	}else if(62 >= windSpeed || windSpeed <= 88){
		console.log(`Tropical storm is detected`);
	}else if(89 >= windSpeed || windSpeed <= 117){
		console.log(`Severe tropical storm is detected`);
	}else{
		console.log(`Cannot identify storm`);
	}

	Sol#2
	function findInten(windSpeed){

		if(windSpeed <= 30){
			console.log(`Not a typhoon`);
		}else if(windSpeed <= 61){
			console.log(`Tropical Depresson is detected`);
		}else if(windSpeed <= 88){
			console.log(`Tropical storm is detected`);
		}else if(windSpeed <= 117){
			console.log(`Severe tropical storm is detected`);
		}else{
			console.log(`Cannot identify storm`);
		}

	}

	findInten(120)

--TERNARY OPERATOR--
		-short hand for if else statement
		- ? = if
		- : = else

		syntax:
		(condition) ? <if statement> : <else statement>


		let myAge = 20;

		myAge<=18 ? console.log(`You are allowed`) : console.log(`You are not allowed here`)

3. SWITCH CASE/STATEMENT
		-complex conditional

		syntax:
		switch (condition){
			case <category>: <statement>
								break;

			default: <statement>
		}

		Sol#1
	 	let num=8
		switch(num){
			case 1: console.log(`1`);
				break;
			case 2: console.log(`2`);	
				break;
			case 3: console.log(`3`);
				break;
			case 4: console.log(`4`);
				break;	
			default: console.log(`not found`);
		}

		Sol#2
		let user=prompt(`role`)
		switch(user){
			case `admin`: console.log(`authorized`);
				break;
			case `employee`: console.log(`ask for authorization`);
				break;
			default: console.log(`no access`);
		}


		let age=5;
		switch(age === 5){
			case true : console.log(`toddler`);
				break;
			default: console.log(`old enough`);
		}

.tolowercase()

D-O-M Sample

	function color(){

		let text = document.getElementById(`input`).value
		let element = document.getElementById(`hello`)

		switch(text){
			case `red` : element.style.color =`red`;
				break;
			case `blue` : element.style.color=`blue`;
				break;
			case `green` : element.style.color=`green`;
				break;
			default: element.style.color=`pink`;
		}

	}




*/


















                                                       